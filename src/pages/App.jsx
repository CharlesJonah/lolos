import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';


// Containers
import Login from './Login/Login';
import SideNav from '../components/common/SideNav/SideNav';
import Campaigns from '../pages/Campaigns/Campaigns';
import CreateNew from '../pages/Campaigns/CreateNew';
import CreateNewStepTwo from '../pages/Campaigns/CreateNewStepTwo';
import Finish from '../pages/Campaigns/Finish';
import CampaignsView from '../pages/Campaigns/CampaignsView';

export const NotFound = () => (
  <h1>Page Not Found</h1>
);

/**
 * Application primary routes
 */
const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Login} />
      <SideNav>
        <Switch>
          <Route path="/campaigns" component={Campaigns} />
          <Route path="/create-new" component={CreateNew} />
          <Route path="/create-new-step-two" component={CreateNewStepTwo} />
          <Route path="/finish" component={Finish} />
          <Route path="/campaigns-view" component={CampaignsView} />
          <Route component={NotFound} />
        </Switch>
      </SideNav>
    </Switch>
  </Router>
);

export default Routes;