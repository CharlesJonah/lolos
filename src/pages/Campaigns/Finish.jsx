import React, { Component } from 'react'
import Footer from '../../components/common/Footer/Footer'
import Preview from '../../assets/images/Preview3.png'
//styling
import '../../styles/pages/Finish.scss'

export default class CreateNew extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <div className="create-new-container">
                <div className='create-new-content'>
                    <div className='campaigns-form'>
                        <div className='preloaded-shoutouts'>
                            <label>PRELOAD SHOUTOUTS WITH # AND @</label>
                            <div className='preloaded-input'>
                                <input type='text' placeholder='for example: #yourbrandname' />
                            </div>
                        </div>
                        <div className='campaign-duration'>
                            <label>SET CAMPAIGN DURATION</label>
                            <div className='campaign-duration-dates'>
                                <input type='text' placeholder='from' id='#dates' />
                                <p> TO </p>
                                <input type='text' placeholder='to' id='#dates' />
                            </div>
                        </div>
                        <div className='company-site'>
                            <label>COMPANY SITE</label>
                            <div className='www'>
                                <input type='text' placeholder='www.' />
                            </div>
                        </div>

                        <div className='company-site'>
                            <label>COMPANY BUDGET</label>
                            <div className='www'>
                                <input type='text' placeholder='500' />
                            </div>
                        </div>

                    </div>
                    <div className='campaigns-img'>
                        <img src={Preview} alt='preview' />
                    </div>
                </div>
                <div className='create-new-footer'>
                    <Footer pageNumber={3} total={3} btnName={'Submit Campaign'} url={'/campaigns-view'} />
                </div>
            </div>
        )
    }
}