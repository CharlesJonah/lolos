import React, { Component } from 'react'
import Footer from '../../components/common/Footer/Footer'
import Preview from '../../assets/images/Preview@3x.png'
//styling
import '../../styles/pages/CreateNew.scss'

export default class CreateNew extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <div className="create-new-container">
                <div className='create-new-content'>
                    <div className='campaigns-form'>
                        <div className='campaign-name'>
                            <label className='campaign-name-label'> CAMPAIGN NAME</label>
                            <input type='text' />
                        </div>
                        <div className='brand-name'>
                            <label className='campaign-name-label'> BRAND LOGO</label>

                            <div className='brand-name-upload'>
                                <div className='brand-name-upload-btn'><p>Upload</p></div>
                            </div>
                        </div>
                        <div className='campaign-punch-line'>
                            <label className='campaign-punch-line-label'> CAMPAIGN PUNCH LINE</label>
                            <input type='text' />
                        </div>
                        <div className='campaign-image'>
                            <label className='campaign-image-label'> CAMPAIGN NAME</label>

                            <div className='campaign-image-upload'>
                                <div className='campaign-image-upload-btn'><p>Upload</p></div>
                            </div>
                        </div>

                    </div>
                    <div className='campaigns-img'>
                        <img src={Preview} alt='preview' />
                    </div>
                </div>
                <div className='create-new-footer'>
                    <Footer pageNumber={1} total={3} btnName={'Next'} url={'/create-new-step-two'} />
                </div>
            </div>
        )
    }
}