import React, { Component } from 'react';
import Modal from '../../components/common/Modal/Modal'
import CampaignLanding from '../../assets/images/CampaignEmpty@3x.png'

import '../../styles/pages/Campaigns.scss'

export default class Campaigns extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
        };
    }
    openModal = () => {
        this.setState({ isModalOpen: true })
    }

    toggleModal = () => {
        this.setState(state => ({
            isModalOpen: !state.isModalOpen
        }));
    }

    render() {

        return (
            <div className='campaigns-page'>
                <p className='header'>YOUR CAMPAIGN LIST IS EMPTY</p>
                <img src={CampaignLanding} alt='campaigns' />
                <p className='lead'>Start with creating your first campaign</p>
                <div className='btn' onClick={this.openModal} ><p>Let’s Start</p></div>
                <Modal isModalOpen={this.state.isModalOpen} toggleModal={this.toggleModal} />
            </div>
        );
    }
}