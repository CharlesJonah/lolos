import React, { Component } from 'react'
import Footer from '../../components/common/Footer/Footer'
import Preview from '../../assets/images/Preview2.png'
//styling
import '../../styles/pages/CreateNewStepTwo.scss'

export default class CreateNew extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <div className="create-new-container">
                <div className='create-new-content'>
                    <div className='campaigns-form'>
                        <div className='about-brand'>
                            <label className='campaign-name-label'> ABOUT BRAND / PRODUCT</label>
                            <textarea placeholder='Few words about the company or the product, 
                                this might give our creators a good background…'
                            />
                        </div>
                        <div className='looking-for'>
                            <label className='campaign-name-label'>WHAT WE ARE LOOKING FOR…</label>
                            <textarea placeholder='Describe your expectations, 
                            what kind of content would you like to get from our creators…'
                            />
                        </div>
                        <div className='special-instructions'>
                            <label className='campaign-name-label'>IS THERE ANY SPECIAL INSTRUCTIONS?</label>
                            <textarea placeholder='This is the place to have all the Doe’s and Don’ts, 
                            it’s not mandatory but will help the creators deliver more valuble Shoutouts…'
                            />
                        </div>
                        

                    </div>
                    <div className='campaigns-img'>
                        <img src={Preview} alt='preview' />
                    </div>
                </div>
                <div className='create-new-footer'>
                    <Footer pageNumber={2} total={3} btnName={'Next'} url={'/finish'} />
                </div>
            </div>
        )
    }
}