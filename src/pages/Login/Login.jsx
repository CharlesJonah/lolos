import React, { Component } from 'react'
import LoginForm from '../../components/Login/LoginForm'

import loginImage from '../../assets/images/LoginImage.jpg';

//styling
import '../../styles/pages/Login.scss'

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="grid-container">
        <div className="login-image-frame">
          <img src={loginImage} alt="logo" width="60" height="60" />
        </div>
        <div className="login-form-frame">
          <LoginForm />
        </div>
      </div>
    )
  }
}