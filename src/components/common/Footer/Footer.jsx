import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import '../../../styles/components/Footer.scss'

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    nextStep = () => {
        this.props.history.push(this.props.url)
    }
    render() {

        return (
            <div className='footer-grid-container'>
                <div className='footer-items'>
                    <div className='footer-span'>
                    </div>
                    <div className='footer-btn-section'>
                        <div className='page-number'>
                            <p>{this.props.pageNumber}/{this.props.total}</p>
                        </div>
                        <div className='footer-btn' onClick={this.nextStep}><p>{this.props.btnName}</p></div>
                    </div>
                </div>
            </div >
        );
    }
}


export default withRouter(Footer);