import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import '../../../styles/components/Modal.scss'

import Content from '../../../assets/images/Content.png'
import Influencer from '../../../assets/images/Influencer.png'

class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    continue = () => {
        this.props.toggleModal();
        this.props.history.push('/create-new')
    }

    render() {
        return (
            <div id="myModal" class="modal" style={(this.props.isModalOpen) ? { display: 'block' } : { display: 'none' }}>
                <div class="modal-content">
                    <p className='modal-header'>SELECT CAMPAIGN TYPE</p>
                    <div className='options'>
                        <img src={Influencer} alt='infuencer' />
                        <img src={Content} alt='content' />
                    </div>
                    <div className='modal-btn'>
                        <div className='cancel' onClick={this.props.toggleModal} ><p>Cancel</p></div>
                        <div className='continue' onClick={this.continue} ><p>Continue</p></div>
                    </div>

                </div>

            </div>
        );
    }
}

export default withRouter(Modal);