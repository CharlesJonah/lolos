import React, { Component } from 'react';
import NavBar from '../NavBar/NavBar'
import Logo from '../../../assets/images/Logo.png'
import CampaignsIcon from '../../../assets/images/Campaigns@3x.png'
import DashboardIcon from '../../../assets/images/Dashboard@3x.png'
import SearchIcon from '../../../assets/images/Search@3x.png'
import LogOutIcon from '../../../assets/images/Logout@3x.png'

import '../../../styles/components/SideNav.scss'

export default class SideNav extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {

        return (
            <div className='side-nav-grid-container'>
                <div className='side-nav'>
                    <div className='header'>
                        <img src={Logo} alt='logo' />
                        <p>Social Media Blasters</p>
                    </div>
                    <div className='menu'>
                        <div className='upper-menu'>
                            <div className='search'>
                                <img src={SearchIcon} alt='search' />
                                <input type='text' placeholder='Search' />
                            </div>
                            <div className='campaigns'>
                                <img src={CampaignsIcon} alt='campaigns' />
                                <p>Campaigns</p>
                            </div>
                            <div className='dashboard'>
                                <img src={DashboardIcon} alt='dashboard' />
                                <p>Dashboard</p>
                            </div>
                        </div>
                        <div className='lower-menu'>
                            <div className='logout'>
                                <img src={LogOutIcon} alt='search' />
                                <p>Log Out</p>
                            </div>
                            <div className='terms-privacy-section'>
                                <p>Our</p>
                                <div className='terms-and-privacy'>
                                    <a href=''> Terms of Use </a> &amp; <a href=''>Privacy Policy </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div >
                <div className='main'>
                    <div className='navbar'>
                        <NavBar />
                    </div>
                    <div className='content'>
                        <div className="section">{this.props.children}</div>
                    </div>
                </div>
            </div >
        );
    }
}