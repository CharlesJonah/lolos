import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import TextField from 'material-ui/TextField';

// styling
import '../../styles/components/LoginForm.scss'

import Logo from '../../assets/images/Logo.png'

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleLogin = () => {
        this.props.history.push('/campaigns')
    }

    render() {

        return (
            <div className='login-form'>
                <img src={Logo} alt='logo' width='50' height='50' />
                <div className='logo-text'>Social Media Blasters</div>
                <TextField
                    hintText='email'
                    fullWidth={true}
                    type='email'
                    errorText={this.state.emailErrorText}
                    underlineFocusStyle={{ "borderColor": "#637280" }}
                    style={{ 'marginTop': '100px' }}
                />
                <TextField
                    hintText='password'
                    fullWidth={true}
                    type='password'
                    errorText={this.state.passwordErrorText}
                    underlineFocusStyle={{ "borderColor": "#637280" }}
                    style={{ 'marginTop': '10px' }}
                />
                <a href=''>Forgot your password?</a>
                <div className='btn' onClick={this.handleLogin} ><p>Log In</p></div>
            </div>
        );
    }
}

export default withRouter(LoginForm);